from django import forms 
from .models import JadwalKegiatan 

class JadwalForm(forms.Form):
    Nama_Jadwal = forms.CharField(max_length = 50)
    Date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
    Time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
    Tempat = forms.CharField(max_length = 50)
    Kategori = forms.CharField(max_length = 50)

