from django.apps import AppConfig


class WebProfileConfig(AppConfig):
    name = 'Web_Profile'
