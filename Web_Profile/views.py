from django.shortcuts import render, redirect 
from django.http import HttpResponse
from Web_Profile.forms import JadwalForm
from Web_Profile.models import JadwalKegiatan

# Create your views here.
# def index(request):
# 	return HttpResponse ("Hello world!") #index itu nama method, request itu parameternya #HttpResponse itu object response 
def Home(request):
    return render (request,'PageHome.html')
def AboutMe(request):
    return render (request, 'PageAboutMe.html')
def Education(request):
    return render (request, 'PageEducation.html')
def Experience(request):
    return render (request, 'PageExperience.html')
def MySkills(request):
    return render (request, 'PageMySkills.html')
def ContactMe(request):
    return render (request, 'PageContactMe.html')
def ActivityKu(request):
    semua_jadwal = JadwalKegiatan.objects.all()
    context = {"jadwal" : semua_jadwal}
    return render (request, 'Activityku.html', context)

def MyActivity(request):
    if request.method == "GET":      #Tampilin form-nya
        form = JadwalForm()
        sent_data = {"jadwal_form" : form}
        return render (request, 'PageMyActivity.html', sent_data)
    else: 
        isi_form = JadwalForm(data=request.POST)     #dict yg isinya data yg telah di submit 
        if isi_form.is_valid():
            new_nama_jadwal = request.POST["Nama_Jadwal"]
            new_date = request.POST["Date"]
            new_time = request.POST["Time"]
            new_tempat = request.POST["Tempat"]
            new_kategori = request.POST["Kategori"]
            
            new_jadwal = JadwalKegiatan(
                Nama_Jadwal = new_nama_jadwal,
                Date = new_date,
                Time = new_time,
                Tempat = new_tempat,
                Kategori = new_kategori
            )
            new_jadwal.save()
            return redirect("/activityku-page/")
        else:
            return HttpResponse("NOT SAVED")

def JadwalDetail(request, jadwal_id):
    nama_object = JadwalKegiatan.objects.get(id = jadwal_id)
    isi_context = {"isinya" : nama_object}
    return render (request, 'DetailJadwal.html', isi_context)

def HapusJadwal(request, hapus_id):
    hapus = JadwalKegiatan.objects.get(id = hapus_id).delete()
    return redirect("/activityku-page/")
