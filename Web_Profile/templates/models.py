from django.db import models
from datetime import datetime

class JadwalKegiatan(models.Model):

    Date = models.DateField()
    Time = models.TimeField()
    Jadwal = models.CharField(max_length = 50)
    Tempat = models.CharField(max_length = 50)
    Kategori = models.CharField(max_length = 50)
