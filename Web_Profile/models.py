from django.db import models
from datetime import datetime


class JadwalKegiatan(models.Model):

    Nama_Jadwal = models.CharField(max_length = 50)
    Date = models.DateField()
    Time = models.TimeField()
    Tempat = models.CharField(max_length = 50)
    Kategori = models.CharField(max_length = 50)

    def __str__(self):
        return self.Nama_Jadwal

