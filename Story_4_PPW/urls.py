"""Story_4_PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Web_Profile import views
from Web_Profile.views import MyActivity

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home-page/', views.Home), #views nama filenya, index itu nama fungsi
    path('aboutme-page/', views.AboutMe),
    path('education-page/', views.Education),
    path('experience-page/', views.Experience),
    path('myskills-page/', views.MySkills),
    path('contactme-page/', views.ContactMe),
    path('myactivity-page/', views.MyActivity),
    path('activityku-page/', views.ActivityKu),
    path('detailjadwal-page/<int:jadwal_id>', views.JadwalDetail),
    path('hapusjadwal-page/<int:hapus_id>', views.HapusJadwal, name="hapus")
]
